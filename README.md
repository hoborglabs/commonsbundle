# Hoborg Commons

Master: [![Build Status](http://ci01.hoborglabs.com/jenkins/job/Hoborg-Commons/badge/icon)](http://ci01.hoborglabs.com/jenkins/job/Hoborg-Commons/)  
Develop: [![Build Status](http://ci01.hoborglabs.com/jenkins/job/Hoborg-Commons-develop/badge/icon)](http://ci01.hoborglabs.com/jenkins/job/Hoborg-Commons-develop/)

Hoborg Commons Bundle and Components (Symfony2 style)

## What's inside

* Components
  * Git wrapper (beta)
  * SVN wrapper (beta)
  * Assetic mustache filter
* Identity service



### To Do

Integrate with existing ACL solution