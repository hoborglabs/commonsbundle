<?php
namespace Hoborg\Component\Assetic;

use \Symfony\Component\Yaml\Yaml;
use \Assetic\FilterManager,
	\Assetic\AssetManager,
	\Assetic\Filter\MustacheFilter,
	\Assetic\Factory\AssetFactory,
	\Assetic\Filter\UglifyJsFilter,
	\Assetic\Filter\Yui\JsCompressorFilter,
	\Assetic\Filter\Yui\CssCompressorFilter;

class Builder {

	protected $root = '.';

	protected $configPath = '';

	protected $config = array();

	public function __construct($publicRoot, $assetConfig) {
		$this->root = $publicRoot;
		$this->configPath = $assetConfig;
	}

	public function buildForPath($path) {
		$config = $this->getConfig();
		$parts = $this->parseAssetPath($path);
		$type = $parts['type'];
		$assetName = $parts['path'];

		if (empty($config[$type][$assetName])) {
			return "/* no assets configuration found for `{$assetName}` */";
		}

		$defaults = array(
			'root' => $this->root,
			'files' => array(),
			'filters' => array()
		);
		$assetConf = $config[$type][$assetName] + $defaults;

		$fm = new FilterManager();
		// register Filters
		$this->registerFilters($fm, $assetConf['filters']);

		$am = new AssetManager();
		$factory = new AssetFactory($assetConf['root']);
		$factory->setAssetManager($am);
		$factory->setFilterManager($fm);

		$asset = $factory->createAsset(
			$assetConf['files'],
			$assetConf['filters']
		);

		return $asset->dump();
	}

	public function setHeaders($path) {
		$parts = $this->parseAssetPath($path);
		$type = $parts['type'];

		switch ($type) {
			case 'styles':
				header('Content-Type: text/css');
				break;

			case 'scripts':
				header('Content-Type: text/javascript');
				break;
		}
	}

	public function getConfig() {
		if (empty($this->config)) {
			$this->config = Yaml::parse($this->configPath);
		}

		return $this->config;
	}

	protected function registerFilters(FilterManager $fm, array $filters) {
		$config = $this->getConfig();

		foreach ($filters as $filterName) {
			if (!empty($config['filters'][$filterName])) {

				// bleh! But I can't thing of anything less ugly.
				$reflection = new \ReflectionClass($config['filters'][$filterName]['class']);
				$filter = call_user_func_array(array(&$reflection, 'newInstance'),
						$config['filters'][$filterName]['params']);

				$fm->set($filterName, $filter);
			}
		}
	}

	protected function parseAssetPath($path) {
		$path = ltrim($path, '/');
		$parts = explode('/', $path);
		$type = array_shift($parts);
		$assetName = implode('/', $parts);

		return array(
			'type' => $type,
			'path' => $assetName,
		);
	}
}
